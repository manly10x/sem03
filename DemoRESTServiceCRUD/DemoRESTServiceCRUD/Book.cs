﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace DemoRESTServiceCRUD
{
    [DataContract]
    public class Book
    {
      [DataMember]
      public int BookId { get; set; }
       
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string ISBN { get; set; }
    }
    public interface IBookRepository
    {
        List<Book> GetAllBooks();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
        bool UpdateABook(Book item);
    }
    public class BookRepository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;

        public BookRepository()
        {
            AddNewBook(new Book { Title = "C# Programming", ISBN = "234223423412" });
            AddNewBook(new Book { Title = "Java Programming", ISBN = "8625239523" });
            AddNewBook(new Book { Title = "WCF Programming", ISBN = "89174812519" });
        }
        public Book AddNewBook(Book newBook)
        {
            if (newBook == null)
                throw new ArgumentNullException("newBook");
            newBook.BookId = counter++;
            books.Add(newBook);
            return newBook;
        }
        public List<Book> GetALlBook()
        {
            return books;
        }
        public bool UpdateABook(Book updateBook)
        {
            if (updateBook == null)
                throw new ArgumentNullException("updatedBook");
            int idx = books.FindIndex(b => b.BookId == updateBook.BookId);
            if (idx == -1)
                return false;
            books.RemoveAt(idx);
            books.Add(updateBook);
            return true;
        }
        public bool DeleteABook(int bookId)
        {
            int idx = books.FindIndex(b => b.BookId == bookId);
            if (idx == -1) 
            return false;
            books.RemoveAll(b => b.BookId == bookId);
            return true;
        }
    }
}