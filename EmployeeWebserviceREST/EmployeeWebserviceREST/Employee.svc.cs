﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Employee" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Employee.svc or Employee.svc.cs at the Solution Explorer and start debugging.
    public class Employee : IEmployee
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        public bool AddEmployee(Employe eml)
        {
            try
            {
                data.Employes.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteEmployee(int idE)
        {
            try
            {
                Employe employeToDelete =
                    (from Employe in data.Employes where Employe.empID == idE select Employe).Single();
                data.Employes.DeleteOnSubmit(employeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Employe> GetProductList()
        {
            try
            {
                return (from Employe in data.Employes select Employe).ToList();
            }
            catch
            {
                return null;
            }
        }
        public bool UpdateEmployee(Employe eml)
        {
            Employe employeToModify =
                (from Employe in data.Employes where Employe.empID == eml.empID select Employe).Single();
            employeToModify.Age = eml.Age;
            employeToModify.Address = eml.Address;
            employeToModify.firstName = eml.firstName;
            employeToModify.LastName = eml.LastName;
            data.SubmitChanges();
            return true;
        }
    }
}
